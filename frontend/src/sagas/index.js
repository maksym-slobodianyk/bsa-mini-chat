import {all} from 'redux-saga/effects'
import chatSagas from '../containers/chat/sagas'
import profileSagas from '../containers/Profile/sagas'


export default function* rootSaga() {
    yield all([chatSagas(),profileSagas()])
}