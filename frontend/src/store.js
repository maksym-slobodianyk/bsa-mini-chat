import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga'
import chatReducer from './containers/chat/reducer';
import rootSaga from './sagas/index'
const initialState = {}

const sagaMiddleware = createSagaMiddleware();

const midlewares = [
    thunk,
    sagaMiddleware
];

const reducers = {
    chat: chatReducer,
};

const rootReducer = combineReducers({
    ...reducers
});

const composeEnhancers = typeof window === 'object'
&& window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;

const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(...midlewares))
);
sagaMiddleware.run(rootSaga);
export default store;
