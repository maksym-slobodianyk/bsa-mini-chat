import { SET_USER } from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = (credentials) =>  dispatch => {
  return {
  type: SET_USER,
  credentials
}};

export const login = credentials => dispatch => {
  dispatch(setUser(credentials));
};

export const loadCurrentUser = () => dispatch => {
  dispatch(setUser());
};

