import { SET_USER_SUCCESS } from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER_SUCCESS:
      return {
        ...state,
        user: action.user,
        isAuthorized: Boolean(action.user?.id),
        isLoading: false
      };
    default:
      return state;
  }
};
