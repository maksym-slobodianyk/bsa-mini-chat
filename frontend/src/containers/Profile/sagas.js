import {all,put, call, takeEvery} from 'redux-saga/effects'
import axios from 'axios'

function postLogin(){
    return axios({
        method: 'get',
        url: `http://localhost:8080/api/auth/login`,
        headers: {},
        body: {}
    })
}
//Have no idea why this action can't be caught
export function* login() {
   const messages = yield call(postLogin);
    yield put({type: 'PROFILE_ACTION:SET_USER_SUCCESS', messages: messages.data})
}

function* watchLogin() {
    yield takeEvery('PROFILE_ACTION:SET_USER', login)
}

export default function* profileSagas() {
    yield all([watchLogin()])
}