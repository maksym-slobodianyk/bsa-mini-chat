import {all, call, put, takeEvery} from 'redux-saga/effects'
import axios from 'axios'

function getAll(){
    return axios({
        method: 'get',
        url: `http://localhost:8080/api/messages/all`,
        headers: {Authorization: `Bearer ${localStorage.getItem('token')}`},
        params: {userId: `bd5ea827-64ff-471c-a5e2-4b4b15dd300c`}
    })
}

export function* fetchMessages() {
    const messages = yield call(getAll);
    yield put({type: 'MESSAGE_ACTION:SET_ALL_MESSAGES_SUCCESS', messages: messages.data})
}

function* watchFetchMessages() {
    console.log("1");
    yield takeEvery('MESSAGE_ACTION:SET_ALL_MESSAGES', fetchMessages)
}

export default function* chatSagas() {
    yield all([watchFetchMessages()])
}