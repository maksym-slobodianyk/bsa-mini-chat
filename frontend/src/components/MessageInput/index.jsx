import React, {useState} from 'react';
import {Form} from 'semantic-ui-react'
import PropTypes from 'prop-types';
import {create_UUID} from "../../services/MessagesService";
import './message-list.css'
import '../../styles/common.css'

function MessageInput({send, updateLastMessage, user}) {
    const [newMessage, setNewMessage] = useState('');

    const handleKeyPress = (event) => {
        if (event.key === "ArrowUp" && newMessage.length === 0) {
            updateLastMessage();
        }
    }

    const handleSendMessage = () => {
        if (newMessage.length && newMessage.trim()) {
            send({
                "id": create_UUID(),
                "userId": user.id,
                "avatar": user.avatar,
                "user": user.name,
                "text": newMessage,
                "createdAt": new Date(),
                "isLiked": false,
                "editedAt": ""
            });
            setNewMessage('');
        }
    }

    return (
        <Form className={"container"} onSubmit={handleSendMessage}>
            <Form.Group className={"form-wrp"} onKeyPress={handleKeyPress}>
                <Form.TextArea
                    className={"field"}
                    placeholder='Write a message...'
                    name='message'
                    value={newMessage}
                    onChange={ev => setNewMessage(ev.target.value)}
                    onKeyDown={handleKeyPress}
                />
                <Form.Button circular color={"olive"} className={"top-btn"} disabled={false} icon={'send'}/>
            </Form.Group>

        </Form>
    );
}

MessageInput.propTypes = {
    send: PropTypes.func.isRequired,
    updateLastMessage: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
};

export default MessageInput;
