package ua.slobodianyk.minechat.message.dto;

import lombok.*;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class MessageCreationDto {

    private String text;
    private UUID userId;
}
