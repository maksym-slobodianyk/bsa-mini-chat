package ua.slobodianyk.minechat.message.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class MessageUpdateDto {

    private UUID id;
    private String text;
}
