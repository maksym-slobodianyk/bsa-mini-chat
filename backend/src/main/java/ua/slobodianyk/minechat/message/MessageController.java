package ua.slobodianyk.minechat.message;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ua.slobodianyk.minechat.message.dto.MessageCreationDto;
import ua.slobodianyk.minechat.message.dto.MessageDto;
import ua.slobodianyk.minechat.message.dto.MessageUpdateDto;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/messages")
public class MessageController {

    private MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/all")
    public List<MessageDto> getAll(@RequestParam UUID userId) {
        return messageService.getAll(userId);
    }

    @GetMapping
    public MessageDto getById(@RequestParam UUID messageId, @RequestParam UUID userId) {
        return messageService.getById(messageId, userId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public void create(@RequestBody MessageCreationDto message) {
        messageService.create(message);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody MessageUpdateDto message) {
        messageService.update(message);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@RequestParam("id") UUID messageId) {
        messageService.delete(messageId);
    }

}
