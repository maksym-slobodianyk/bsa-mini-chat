package ua.slobodianyk.minechat.auth.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import ua.slobodianyk.minechat.user.dto.UserDto;

@Data
@AllArgsConstructor
public class AuthUserDTO {
    private String token;
    private UserDto user;
}