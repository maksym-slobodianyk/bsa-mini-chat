package ua.slobodianyk.minechat.auth.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class UserRegisterDto {
    private String email;
    private String password;
    private String username;
    private String avatar;
    private UUID roleId;
}