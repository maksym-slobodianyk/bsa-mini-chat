package ua.slobodianyk.minechat.user.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import ua.slobodianyk.minechat.userRole.model.UserRole;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "users")

public class User {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private String avatar;

    @Column(unique = true)
    private String email;

    @Column
    private String password;

    @Column
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "role_id")
    private UserRole role;
}
