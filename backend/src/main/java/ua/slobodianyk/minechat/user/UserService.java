package ua.slobodianyk.minechat.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ua.slobodianyk.minechat.auth.model.AuthUser;
import ua.slobodianyk.minechat.user.dto.UserDto;
import ua.slobodianyk.minechat.user.dto.UserUpdateDto;
import ua.slobodianyk.minechat.user.model.User;
import ua.slobodianyk.minechat.userRole.model.UserRole;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> u = userRepository.findByEmail(username);
        UserDetails ud = u
                .map(user -> new AuthUser(user.getId(), user.getEmail(), user.getPassword()))
                .orElseThrow(() -> new UsernameNotFoundException(username));
        return ud;
    }

    public List<UserDto> getAll() {
        return ((List<User>) userRepository.findAll())
                .stream()
                .map(u -> UserDto.builder()
                        .id(u.getId())
                        .user(u.getName())
                        .avatar(u.getAvatar())
                        .roleId(u.getRole().getId())
                        .roleName(u.getRole().getName())
                        .email(u.getEmail())
                        .build())
                .collect(Collectors.toList());
    }

    public UserDto getById(UUID userId) throws ChangeSetPersister.NotFoundException {
        User response = userRepository.findById(userId).orElseThrow(ChangeSetPersister.NotFoundException::new);
        return UserDto.builder()
                .id(response.getId())
                .user(response.getName())
                .avatar(response.getAvatar())
                .roleId(response.getRole().getId())
                .roleName(response.getRole().getName())
                .email(response.getEmail())
                .build();
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public void update(UserUpdateDto userDto) throws ChangeSetPersister.NotFoundException {
        User userToUpdate = userRepository.findById(userDto.getId()).orElseThrow(ChangeSetPersister.NotFoundException::new);
        userToUpdate.setAvatar(userDto.getAvatar());
        userToUpdate.setName(userDto.getUser());
        userToUpdate.setEmail(userDto.getEmail());
        userToUpdate.setRole(UserRole.builder()
                .id(userDto.getRoleId())
                .build());
        userRepository.save(userToUpdate);
    }

    public void deleteById(UUID userId) {
        userRepository.deleteById(userId);
    }
}
