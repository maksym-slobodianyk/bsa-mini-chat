package ua.slobodianyk.minechat.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class UserDto {

    private UUID id;
    private String avatar;
    private String user;
    private String email;
    private String password;
    private UUID roleId;
    private String roleName;

}
