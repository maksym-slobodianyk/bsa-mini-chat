package ua.slobodianyk.minechat.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;


@Data
@AllArgsConstructor
@NoArgsConstructor

public class UserUpdateDto {

    private UUID id;
    private String avatar;
    private String user;
    private UUID roleId;
    private String email;
}

