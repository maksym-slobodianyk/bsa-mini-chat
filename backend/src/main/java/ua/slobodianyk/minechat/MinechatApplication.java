package ua.slobodianyk.minechat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class MinechatApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinechatApplication.class, args);
	}

}
