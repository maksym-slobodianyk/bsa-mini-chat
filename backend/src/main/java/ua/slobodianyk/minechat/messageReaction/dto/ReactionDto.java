package ua.slobodianyk.minechat.messageReaction.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class ReactionDto {

    private UUID userId;
    private UUID messageId;
}
