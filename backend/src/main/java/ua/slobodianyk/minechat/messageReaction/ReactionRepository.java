package ua.slobodianyk.minechat.messageReaction;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.slobodianyk.minechat.messageReaction.model.MessageReaction;


import java.util.Optional;
import java.util.UUID;

@Repository
public interface ReactionRepository extends CrudRepository<MessageReaction, UUID> {

    public Optional<MessageReaction> findByMessageIdAndUserId(UUID messageId, UUID userId);

    public void deleteByMessageIdAndUserId(UUID messageId, UUID userId);
}
