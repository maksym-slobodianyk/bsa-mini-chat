package ua.slobodianyk.minechat.userRole;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.slobodianyk.minechat.userRole.model.UserRole;

import java.util.List;

@RestController
@RequestMapping("/api/roles")
public class UserRoleController {

    private UserRoleService userRoleService;

    @Autowired
    public UserRoleController(UserRoleService userRoleService) {
        this.userRoleService = userRoleService;
    }

    @GetMapping
    public List<UserRole> getAll() {
        return userRoleService.getAll();
    }
}
