package ua.slobodianyk.minechat.userRole;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.slobodianyk.minechat.userRole.model.UserRole;

import java.util.UUID;

@Repository
public interface UserRoleRepository extends CrudRepository<UserRole, UUID> {
}
