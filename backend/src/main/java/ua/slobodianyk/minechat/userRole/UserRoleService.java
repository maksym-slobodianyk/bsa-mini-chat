package ua.slobodianyk.minechat.userRole;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.slobodianyk.minechat.userRole.model.UserRole;

import java.util.List;


@Service
public class UserRoleService {

    private UserRoleRepository userRoleRepository;

    @Autowired
    public UserRoleService(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    public List<UserRole> getAll() {
        return (List<UserRole>) userRoleRepository.findAll();
    }
}
